{
  nordAuroraColors = [
    "rgba(191,  97, 106, 0.2)" # #BF616A (Aurora)
    "rgba(208, 135, 112, 0.2)" # #D08770 (Aurora)
    "rgba(235, 203, 139, 0.2)" # #EBCB8B (Aurora)
    "rgba(163, 190, 140, 0.2)" # #A3BE8C (Aurora)
    "rgba(180, 142, 173, 0.2)" # #B48EAD (Aurora)
  ];
  nordFrostColors = [
    "rgba(143, 188, 187, 0.2)" # #8FBCBB (Frost)
    "rgba(136, 192, 208, 0.2)" # #88C0D0 (Frost)
    "rgba(129, 161, 193, 0.2)" # #81A1C1 (Frost)
    "rgba(94,  129, 172, 0.2)" # #5E81AC (Frost)
  ];
  nordFrostColorsSaturated = [
    "rgba(143, 188, 187, 0.6)" # #8FBCBB (Frost)
    "rgba(136, 192, 208, 0.6)" # #88C0D0 (Frost)
    "rgba(129, 161, 193, 0.6)" # #81A1C1 (Frost)
    "rgba(94,  129, 172, 0.6)" # #5E81AC (Frost)
  ];

  nordPolarNightColors = [
    "rgba(46, 52, 64,  0.2)" # #2E3440 (Polar Night)
    "rgba(59, 66, 82,  0.2)" # #3B4252 (Polar Night)
    "rgba(67, 76, 94,  0.2)" # #434C5E (Polar Night)
    "rgba(76, 86, 106, 0.2)" # #4C566A (Polar Night)
  ];

  nordSnowStormColors = [
    "rgba(216, 222, 233, 0.2)" # #D8DEE9 (Snow Storm)
    "rgba(229, 233, 240, 0.2)" # #E5E9F0 (Snow Storm)
    "rgba(236, 239, 244, 0.2)" # #ECEFF4 (Snow Storm)
  ];

  nordStatusColors = [
    # npmjs.com/package/@nordhealth/themes/lib/nord-dark-high-contrast.css
    "rgba(52, 57, 66, 0.2)" # status-neutral
    "rgba(255, 205, 66, 0.2)" # status-warning
    "rgba(156, 68, 233, 0.2)" # status-highlight
    "rgba(255, 64, 26, 0.2)" # status-danger
    "rgba(7, 156, 36, 0.2)" # status-success
    "rgba(29, 80, 247, 0.2)" # status-info
    "rgba(255, 64, 26, 0.2)" # status-notification
    "rgba(57, 64, 65, 0.2)" # status-neutral-weak
    "rgba(97, 71, 0, 0.2)" # status-warning-weak
    "rgba(83, 21, 111, 0.2)" # status-highlight-weak
    "rgba(115, 25, 12, 0.2)" # status-danger-weak
    "rgba(20, 92, 41, 0.2)" # status-success-weak
    "rgba(18, 39, 120, 0.2)" # status-info-weak
    "rgba(15, 83, 97, 0.2)" # status-progress-weak
  ];
}